#ifndef TPulseFitter_h
#define TPulseFitter_h

#include "TTree.h"
#include "TH1D.h"
#include "TF1.h"

class PulseFitterResult{

public:

  double fittedTime;

};



class TPulseFitter  {

public:

  TPulseFitter(){
    
   }
  

  // ShapeType
  // 0 -> signal HPD
  // 1 -> reference 100MHz, 1.5ns sampler
  PulseFitterResult FitPulse(TH1D *waveform, int ch, double pulseHeight, double peakTime, double baseline, int shapeType);


  void EndRun(){
    
    
  }
  
private:

};

#endif

