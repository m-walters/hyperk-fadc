#ifndef TPulseFinder_h
#define TPulseFinder_h

#include "TTree.h"
#include "TH1D.h"

class PulseFinderResult{

public:
  bool foundPulse;
  int pulseHeight; // in counts
  int pulseHeightmV; // in mV
  double pulseHeightPE; // in PE
  double pulseTime; // in ns
  double pulseCharge; // integral
  double baseline;

};


// When giving a waveform, class will perform the
// following analysis:
//  i) calculate the integral in the pulse region
//  ii) look for pulses below threshold in pulse region.
//  iii) measure average baseline before pulse region
//  iv) fill baseline histogram before pulse region.
class TPulseFinder  {

public:

  TPulseFinder(){
    
    pulseWindow[0] = 4300;
    pulseWindow[1] = 4400;
  }
  
  PulseFinderResult FindPulses(TH1D *waveform, double baseline, TH1F *baseline_histo = 0){

    PulseFinderResult result;

    int minPulseHeight = 999999;
    float pulseCharge = 0;
    result.pulseTime = -9999;

    // Loop over the bins in waveform.
    for(int i = 1; i < waveform->GetNbinsX(); i++){

      double x = waveform->GetBinCenter(i);
      double bin_value = waveform->GetBinContent(i);
      
      // Fill the baseline histogram
      if(baseline_histo && x < pulseWindow[0]){
	baseline_histo->Fill(waveform->GetBinContent(i));
      }

      // In pulse region
      if(x > pulseWindow[0] && x < pulseWindow[1]){
	
	if(bin_value < minPulseHeight){
	  minPulseHeight = bin_value;
	  result.pulseTime = x;
	}

	pulseCharge += baseline - bin_value;

      }

    }

    result.pulseHeight = baseline - minPulseHeight;
    result.pulseHeightPE = result.pulseHeight/45.0;

    result.pulseCharge = pulseCharge;
    result.baseline = baseline;

    return result;

  }

  void EndRun(){
    
	
  }

private:

  // Define the window inside which we will look for pulses.
  double pulseWindow[2];


};

#endif

