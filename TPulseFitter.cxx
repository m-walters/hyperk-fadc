#include "TPulseFitter.hxx"

#include <cmath>
#include "TROOT.h"



double funcResp(double* x, double* par){
  double tDiff = (x[0]-par[2]);
  tDiff/=par[3];
  return tDiff<0. ? par[1] : 
    par[1]+par[0]*tDiff*tDiff*tDiff*tDiff*(1.-tDiff/par[4])*exp(-tDiff);
}


PulseFitterResult TPulseFitter::FitPulse(TH1D *waveform, int ch,
					 double pulseHeight, double peakTime, double baseline, int shapeType){
  
  PulseFitterResult result;
  

  double range_low, range_high;
  if(shapeType == 0){
    range_low = 35;
    range_high = 50;
  }else if(shapeType == 1){
    range_low = 50;
    range_high = 25;
  }
    

  char name[100];
  sprintf(name,"pulse_timing_%i",ch);
  // Let's delete this TF1 before recreating it.
  TF1 *old = (TF1*)gROOT->Get(name);
  if(old){
    delete old;
  }
  
  TF1 *fitted_pulse = new TF1(name,funcResp,
			      peakTime-range_low,peakTime+range_high,5);
  
  fitted_pulse->SetParameter(0,-pulseHeight * 0.24);
  fitted_pulse->SetParameter(1,baseline);
  fitted_pulse->SetParameter(2,peakTime-28);

  if(shapeType == 0){
    fitted_pulse->SetParameter(3,9.68);
    fitted_pulse->SetParameter(4,260);
  }else if (shapeType == 1){
    fitted_pulse->SetParameter(3,8.30);
    fitted_pulse->SetParameter(4,130.6);    
  }
      
  fitted_pulse->SetLineColor(2);
  
  
  waveform->Fit(name,"RQ","",peakTime-range_low, peakTime+range_high);
  
  
    
  // Let's go ahead and convert fitted time to ns.
  //  double fitted_time = fitted_pulse->GetParameter(2);
  
  if(pulseHeight > 30){
    static double avg  = 0;
    static double avgn = 0;
    
    avg += fitted_pulse->GetParameter(4);
    avgn += 1.0;
    
    //    std::cout << "FFF " <<fitted_pulse->GetParameter(4) << " " << pulseHeight << " " <<  avg/avgn << " " << avg << std::endl;
  }

  result.fittedTime =  fitted_pulse->GetParameter(2);

  return result;
  
  
}
