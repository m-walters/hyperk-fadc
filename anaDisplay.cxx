#include <stdio.h>
#include <iostream>

#include "TRootanaDisplay.hxx"
#include "TH1D.h"
#include "TV792Data.hxx"

#include "TFancyHistogramCanvas.hxx"


#include "TAnaManager.hxx"
#include "TMulticanvas.h"

class MyTestLoop: public TRootanaDisplay { 

public:
	
	// An analysis manager.  Define and fill histograms in 
	// analysis manager.
	TAnaManager *anaManager;

  MyTestLoop() {
    SetOutputFilename("example_output");
    DisableRootOutput(false);

    anaManager = new TAnaManager();
		
  }

  bool CheckOption(std::string option)
  {
    return anaManager->CheckOption(option);
  }

  void AddAllCanvases(){

    SetNumberSkipEvent(1);
    // Set up tabbed canvases


		if(anaManager->HaveV792Histograms()) 
			AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetV792Histograms(),"V792"));

		if(anaManager->HaveV1190Histograms()) 
			AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetV1190Histograms(),"V1190"));

		if(anaManager->HaveL2249Histograms()) 
			AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetL2249Histograms(),"L2249"));

		if(anaManager->HaveAgilentistograms()) 
			AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetAgilentistograms(),"AGILENT"));

		if(anaManager->HaveV1720Histograms()) 
			AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetV1720Histograms(),"V1720 Waveforms"));

		if(anaManager->HaveV1730DPPistograms()) 
			AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetV1730DPPistograms(),"V1730 Waveforms"));

		if(anaManager->HaveV1730Rawistograms()) 
			AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetV1730Rawistograms(),"V1730 Waveforms"));

		if(anaManager->HaveDT724Histograms()) 
			AddSingleCanvas(new TFancyHistogramCanvas(anaManager->GetDT724Histograms(),"DT724 Waveforms"));

		TMulticanvas *baselines = new TMulticanvas("Baselines");
		baselines->AddHistoSingle(anaManager->signalBaseline,0);
		AddSingleCanvas(baselines);

		TMulticanvas *pulseFinder = new TMulticanvas("Pulse Heights");
		pulseFinder->AddHistoSingle(anaManager->signalPulseHeight,0);
		pulseFinder->AddHistoSingle(anaManager->signalPulseCharge,2);
		AddSingleCanvas(pulseFinder);

		/*		TMulticanvas *fitTime = new TMulticanvas("Fitted Times 0");
		fitTime->AddHistoSingle(anaManager->tdiff_vs_pe[1],0);
		fitTime->AddHistoSingle(anaManager->tdiff_vs_pe[3],1);
		fitTime->AddHistoSingle(anaManager->tdiff_vs_pe[5],2);
		fitTime->AddHistoSingle(anaManager->tdiff_vs_pe[7],3);
		AddSingleCanvas(fitTime);

		TMulticanvas *fitTime1 = new TMulticanvas("Fitted Times 1");
		fitTime1->AddHistoSingle(anaManager->tdiff_vs_pe[9],0);
		fitTime1->AddHistoSingle(anaManager->tdiff_vs_pe[11],1);
		fitTime1->AddHistoSingle(anaManager->tdiff_vs_pe[13],2);
		fitTime1->AddHistoSingle(anaManager->tdiff_vs_pe[15],3);
		AddSingleCanvas(fitTime1);

		TMulticanvas *fitTime2 = new TMulticanvas("Fitted Times 2");
		fitTime2->AddHistoSingle(anaManager->tdiff_vs_pe[17],0);
		fitTime2->AddHistoSingle(anaManager->tdiff_vs_pe[19],1);
		fitTime2->AddHistoSingle(anaManager->tdiff_vs_pe[21],2);
		fitTime2->AddHistoSingle(anaManager->tdiff_vs_pe[23],3);
		AddSingleCanvas(fitTime2);

		TMulticanvas *fitTime3 = new TMulticanvas("Fitted Times 3");
		fitTime3->AddHistoSingle(anaManager->tdiff_vs_pe[25],0);
		fitTime3->AddHistoSingle(anaManager->tdiff_vs_pe[27],1);
		fitTime3->AddHistoSingle(anaManager->tdiff_vs_pe[29],2);
		fitTime3->AddHistoSingle(anaManager->tdiff_vs_pe[31],3);
		AddSingleCanvas(fitTime3);

		TMulticanvas *fitTime4 = new TMulticanvas("Fitted Times 4");
		fitTime4->AddHistoSingle(anaManager->tdiff_vs_pe[33],0);
		fitTime4->AddHistoSingle(anaManager->tdiff_vs_pe[35],1);
		fitTime4->AddHistoSingle(anaManager->tdiff_vs_pe[37],2);
		fitTime4->AddHistoSingle(anaManager->tdiff_vs_pe[39],3);
		AddSingleCanvas(fitTime4);
		*/		


    SetDisplayName("Example Display");
  };

  virtual ~MyTestLoop() {};

  void ResetHistograms(){}

  void UpdateHistograms(TDataContainer& dataContainer){

		anaManager->ProcessMidasEvent(dataContainer);
	}

  void PlotCanvas(TDataContainer& dataContainer){}

  void EndRun(int transition,int run,int time){    
    anaManager->EndOfRun();
  }

}; 






int main(int argc, char *argv[])
{
  MyTestLoop::CreateSingleton<MyTestLoop>();  
  return MyTestLoop::Get().ExecuteLoop(argc, argv);
}

