#include "TAnaManager.hxx"


TAnaManager::TAnaManager(TAnaManagerConfig *config){


  if(!config){
    Configuration = new TAnaManagerConfig();
  }else{
    Configuration = new TAnaManagerConfig(*config);
  }  

	fV792Histogram = 0;
#ifdef USE_V792
	fV792Histogram = new TV792Histograms();
	fV792Histogram->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fV1190Histogram = 0;
#ifdef USE_V1190
	fV1190Histogram = new TV1190Histograms();
	fV1190Histogram->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fL2249Histogram = 0;
#ifdef USE_L2249
  fL2249Histogram = new TL2249Histograms();
	fL2249Histogram->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fAgilentHistogram = 0;
#ifdef USE_AGILENT
	fAgilentHistogram = new TAgilentHistograms();
	fAgilentHistogram->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fV1720Waveform = 0;
#ifdef USE_V1720
	fV1720Waveform = new TV1720Waveform();
	fV1720Waveform->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fV1730DppWaveform = 0;
#ifdef USE_V1730DPP
  fV1730DppWaveform = new TV1730DppWaveform();
	fV1730DppWaveform->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fV1730RawWaveform = 0;
#ifdef USE_V1730RAW
	fV1730RawWaveform = new TV1730RawWaveform();
	fV1730RawWaveform->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	fDT724Waveform = 0;
#ifdef USE_DT724
	fDT724Waveform = new TDT724Waveform();
	fDT724Waveform->DisableAutoUpdate();  // disable auto-update.  Update histo in AnaManager.
#endif

	
	// Make a tree of interesting variables.
	fOutputTree = new TTree("hkTimingTree","A simple summary tree");
	fOutputTree->Branch("PulseHeightSignal", &fPulseHeightSignal, "PulseHeightSignal/F");
	fOutputTree->Branch("PulseHeightReference", &fPulseHeightReference, "PulseHeightReference/F");
	fOutputTree->Branch("PulseChargeSignal", &fPulseChargeSignal, "PulseChargeSignal/F");
	fOutputTree->Branch("PulseChargeReference", &fPulseChargeReference, "PulseChargeReference/F");
	fOutputTree->Branch("PulseTimeSignal", &fPulseTimeSignal, "PulseTimeSignal/F");
	fOutputTree->Branch("PulseTimeReference", &fPulseTimeReference, "PulseTimeReference/F");

	fPulseFinder = new TPulseFinder();
	fPulseFitter = new TPulseFitter();

	// Create histograms for storing information on signal and
	// and reference baseline measurements.
	signalBaseline = new TH1F("sigBase","Signal Baseline",100,14800,15500);
	referenceBaseline = new TH1F("refBase","Reference Baseline",100,14800,15500);

	signalPulseHeight = new TH1F("signalPulseHeight","Signal Pulse Height",1100,-500,5000);
	referencePulseHeight = new TH1F("referencePulseHeight","Reference Pulse Height",1100,-500,5000);

	signalPulseCharge = new TH1F("signalPulseCharge","Signal Pulse Charge",200,-1000,10000);
	referencePulseCharge = new TH1F("referencePulseCharge","Reference Pulse Charge",200,-1000,10000);

	tdiff_sig_ref = new TH1F("tdiff_sig_ref","Time Difference: Signal - Reference",200,-20,20);

	for(int i = 0; i< 40; i++){
	  char name[100];
	  double low = i*0.5 + 0.25;
	  double high = low + 0.5;
	  sprintf(name,"tdiff_%f_%f",low,high);
	  tdiff_vs_pe[i] = new TH1F(name,name,400,-15,15);
	}
};



int TAnaManager::ProcessMidasEvent(TDataContainer& dataContainer){
	
	if(fV792Histogram) fV792Histogram->UpdateHistograms(dataContainer); 
	if(fV1190Histogram)  fV1190Histogram->UpdateHistograms(dataContainer); 
	if(fL2249Histogram)  fL2249Histogram->UpdateHistograms(dataContainer); 
	if(fAgilentHistogram)  fAgilentHistogram->UpdateHistograms(dataContainer); 
	if(fV1720Waveform)  fV1720Waveform->UpdateHistograms(dataContainer); 
	if(fV1730DppWaveform)  fV1730DppWaveform->UpdateHistograms(dataContainer); 
	if(fV1730RawWaveform)  fV1730RawWaveform->UpdateHistograms(dataContainer); 
	if(fDT724Waveform)  fDT724Waveform->UpdateHistograms(dataContainer); 

	// Choose the signal and reference waveform.  Need the right digitizer and 
	// channel number.
	TH1D *sig_waveform;
	TH1D *ref_waveform;
	if(Configuration->UseV1730){
	  sig_waveform = (TH1D*)(*fV1730RawWaveform)[1];
	  ref_waveform = (TH1D*)(*fV1730RawWaveform)[0];
	}else if(Configuration->UseV1720){
	  sig_waveform = (TH1D*)(*fV1720Waveform)[1];
	  ref_waveform = (TH1D*)(*fV1720Waveform)[0];	  
	}else{
	  sig_waveform = (TH1D*)(*fDT724Waveform)[1];
	  ref_waveform = (TH1D*)(*fDT724Waveform)[0];	  	  
	}


	// Find the signal pulse.
	PulseFinderResult result = 
	  fPulseFinder->FindPulses(sig_waveform,15065,signalBaseline);
	//fPulseFinder->FindPulses(sig_waveform,14960,signalBaseline);
	
	signalPulseHeight->Fill(result.pulseHeight);
	fPulseHeightSignal = result.pulseHeight;

	signalPulseCharge->Fill(result.pulseCharge);
	fPulseChargeSignal = result.pulseCharge;

	// Fit the signal pulse
	PulseFitterResult fitResult = 
	  fPulseFitter->FitPulse(sig_waveform, 1, 
				 result.pulseHeight, result.pulseTime, result.baseline, 0);

	// Find and fit reference pulse.
	PulseFinderResult result2 = 
	  fPulseFinder->FindPulses(ref_waveform,14950);
	
	//signalPulseHeight->Fill(result.pulseHeight);
	fPulseHeightReference = result2.pulseHeight;

	//signalPulseCharge->Fill(result.pulseCharge);
	fPulseChargeReference = result2.pulseCharge;

	// Fit the signal pulse
	PulseFitterResult fitResult2 = 
	  fPulseFitter->FitPulse(ref_waveform, 0, 
				 result2.pulseHeight, result2.pulseTime, result2.baseline, 1);
	
	//	tdiff_sig_ref

	fPulseTimeSignal = fitResult.fittedTime;
	fPulseTimeReference = fitResult2.fittedTime;

	tdiff_sig_ref->Fill(fPulseTimeSignal-fPulseTimeReference);

	double pe = result.pulseHeightPE;
	int pe_bin = (pe-0.25)*2;
	if(pe> 8 && 0) 
	  std::cout << "bin " << pe << " " << pe_bin << " " << fPulseTimeSignal << 
	    " " << fPulseTimeReference << " " 
		    << fPulseTimeSignal - fPulseTimeReference << std::endl;
	
	if(pe_bin >= 0 && pe_bin < 40)
	  tdiff_vs_pe[pe_bin]->Fill(fPulseTimeSignal-fPulseTimeReference);

	fOutputTree->Fill();

	return 1;
}



bool TAnaManager::HaveV792Histograms(){
	if(fV792Histogram) return true; 
	return false;
}
bool TAnaManager::HaveV1190Histograms(){
	if(fV1190Histogram) return true; 
	return false;
};
bool TAnaManager::HaveL2249Histograms(){
	if(fL2249Histogram) return true; 
	return false;
};
bool TAnaManager::HaveAgilentistograms(){
	if(fAgilentHistogram) return true; 
	return false;
};
bool TAnaManager::HaveV1720Histograms(){
	if(fV1720Waveform) return true; 
	return false;
};
bool TAnaManager::HaveV1730DPPistograms(){
	if(fV1730DppWaveform) return true; 
	return false;
};
bool TAnaManager::HaveV1730Rawistograms(){
	if(fV1730RawWaveform) return true; 
	return false;
};
bool TAnaManager::HaveDT724Histograms(){
	if(fDT724Waveform) return true; 
	return false;
};

TV792Histograms* TAnaManager::GetV792Histograms() {return fV792Histogram;}
TV1190Histograms* TAnaManager::GetV1190Histograms(){return fV1190Histogram;}
TL2249Histograms* TAnaManager::GetL2249Histograms(){return fL2249Histogram;}
TAgilentHistograms* TAnaManager::GetAgilentistograms(){return fAgilentHistogram;}
TV1720Waveform* TAnaManager::GetV1720Histograms(){return fV1720Waveform;}
TV1730DppWaveform* TAnaManager::GetV1730DPPistograms(){return fV1730DppWaveform;}
TV1730RawWaveform* TAnaManager::GetV1730Rawistograms(){return fV1730RawWaveform;}
TDT724Waveform* TAnaManager::GetDT724Histograms(){return fDT724Waveform;}

